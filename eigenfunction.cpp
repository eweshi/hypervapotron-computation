#include "eigenfunction.h"


eigenfunction1_1::eigenfunction1_1(Hypervapotron &hyp)
{
    t = hyp.t;
    Bi = hyp.Bi_1;
}

eigenfunction3_1::eigenfunction3_1(Hypervapotron &hyp)
{
    x1 = hyp.x1;
    Bi = hyp.Bi_2;
}

eigenfunction5_1::eigenfunction5_1(Hypervapotron &hyp)
{
    H1 = hyp.H_1;
    Bi = hyp.Bi_3;
}