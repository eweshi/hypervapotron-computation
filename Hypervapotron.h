#ifndef HYPERVAPOTRON_H
#define HYPERVAPOTRON_H
#include <vector>
#include <math.h>
#include "eigenfunction.h"
#include "HypParameters.h"
#include <boost/math/tools/roots.hpp>

#define  ARMA_DONT_USE_WRAPPER
#include <armadillo>

using namespace std;
using namespace arma;

class eigenfunction1_1;
class eigenfunction3_1;
class eigenfunction5_1;

class Hypervapotron
{
public:

    Hypervapotron(int, HypParameters);
    ~Hypervapotron();


    void setParameters();


    void generateLambda1_1(eigenfunction1_1);
    void generateLambda1_2();
    void generateLambda2_1();
    void generateLambda2_2();
    void generateLambda2_3();
    void generateLambda3_1(eigenfunction3_1);
    void generateLambda3_2(eigenfunction3_1);
    void generateLambda3_3();
    void generateLambda4_1();
    void generateLambda4_2();
    void generateLambda5_1(eigenfunction5_1);


    //Domain Coefficient and Constant functions for matricies.
    float domain1_1coff(int);
    float domain1_2eq(int n);
    float domain2_1coeff(int);
    float domain2_1const(int);
    float domain2_2_const(int);
    float domain2_3_1coeff(int);
    float domain2_3_2coeff(int);
    float domain3_1_1coeff(int);
    float domain3_1_2const(int);
    float domain3_1_3coeff(int);
    float domain3_2_1coeff(int);
    float domain3_2_2coeff(int);
    float domain4_1_1coeff(int);
    float domain4_1_2coeff(int);
    float domain4_2_1coeff(int);
    float domain5_1_1coeff(int);
    float domain5_1_2coeff(int);

        //Domain Sequencing functions
    float sequence_domain1_1coff(int);
    float sequence_domain1_2eq(int n);
    float sequence_domain2_1coeff(int);
    float sequence_domain2_1const(int);
    float sequence_domain2_2_const( float lambda2_2);
    float sequence_domain2_3_1coeff(float y);
    float sequence_domain2_3_2coeff(float y);
    float sequence_domain3_1_1coeff(int);
    float sequence_domain3_1_2const(int);
    float sequence_domain3_1_3coeff(int);
    float sequence_domain3_2_1coeff(int);
    float sequence_domain3_2_2coeff(int);
    float sequence_domain4_1_1coeff(int);
    float sequence_domain4_1_2coeff(int);
    float sequence_domain4_2_1coeff(int);
    float sequence_domain5_1_1coeff(int);
    float sequence_domain5_1_2coeff(int);

    //matrix construction
    void makeRow1(int index);
    void makeRow2(int index);
    void makeRow3(int index);
    void makeRow4(int index);
    void makeRow5(int index);
    void makeRow6(int index);
    void makeRow7(int index);
    void makeRow8(int index);
    void makeMatrix(int index);
    
    void solveCoefficients(eigenfunction1_1,eigenfunction3_1,eigenfunction5_1);



    //root finding methods
    float lambda1_1rootfind(int index,eigenfunction1_1);
    float lambda3_1rootfind(int index,eigenfunction3_1);
    float lambda5_1rootfind(int index,eigenfunction5_1);

    //Computation
    void beginComputation(eigenfunction1_1,eigenfunction3_1,eigenfunction5_1);

  float w;
  float t;

  float w_c;
  float H;
  float H_c;
  float H_1;
  float Bi_1;
  float Bi_2;
  float Bi_3;
  float Bi_wc_n;
  float h_eq;
  float h_fin;
  float finEfficiency;
  float Area_totSurf;
  float Area_flowChannel;
  float h1;
  float h2;
  float k1;
  float k2;
  float k3;
  float k4;
  float k5;
  float h3;

  float T_ref;
  float x1;
  float y1;
  float qdoubleprime_infinity;

protected:
    
private:
    int colLoc;
    int row;
    int col;
    
    int lamb1_1Count;
    int lamb1_2Count;
    int lamb2_1Count;
    int lamb2_2Count;
    int lamb2_3_1Count;
    int lamb2_3_2Count;
    int lamb3_1Count;
    int lamb3_2Count;
    int lamb3_3Count;
    int lamb4_1Count;
    int lamb4_2Count;
    int lamb5_1Count;

  //Computation Specifics
  const int convergeNum;
  const int maxIndex;

  //Data Arrays
  
  //Lambda Arrays
    vector<float> lambda1_1Vector;
    vector<float> lambda1_2Vector;
    vector<float> lambda2_1Vector;
    vector<float> lambda2_2Vector;
    vector<float> lambda2_3Vector;
    vector<float> lambda3_1Vector;
    vector<float> lambda3_2Vector;
    vector<float> lambda3_3Vector;
    vector<float> lambda4_1Vector;
    vector<float> lambda4_2Vector;
    vector<float> lambda5_1Vector;
    
    //Holding Arrays
    vec B; //That is, A*B = X;
    vector<float> d1_1Vec;
    vector<float> d1_2Vec;
    vector<float> d2_1Vec;
    vector<float> d2_2Vec;
    vector<float> d2_3Vec;
    vector<float> d3_1Vec;
    vector<float> d3_2Vec;
    vector<float> d4_1Vec;
    vector<float> d4_2Vec;
    vector<float> d5_1Vec;
    
    mat coefficientMat;
    vec constMat; //Column constant vector
    
    //private functions
    void fillMatrixSystem();
    void solveSystem();
    void fillSection1();
    void fillSection2();
    void fillSection3();
    void fillSection4();
    void fillSection5();
    void fillSection6();
    void fillSection7();
    void fillSection8();
    
    void generateLambdaVectors(eigenfunction1_1,eigenfunction3_1,eigenfunction5_1);
    
    void fillDomain1_2();
    void fillDomain2_2();
};

#endif // HYPERVAPOTRON_H