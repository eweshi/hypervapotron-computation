#ifndef EIGENFUNCTION_H
#define EIGENFUNCTION_H
#include "Hypervapotron.h"

class Hypervapotron;

class eigenfunction1_1
{
    public:

    eigenfunction1_1(Hypervapotron &hyp);
    double t;
    double Bi;

    double operator() (const double x)
    {
        return cos(t*x) - ((sin(t*x)*(x*t))/Bi);
    }
};

class eigenfunction3_1
{
    public:
    eigenfunction3_1(Hypervapotron &hyp);
    double x1;
    double Bi;
    double operator() (const double x)
    {
        return cos(x1*x) - ((sin(x1*x)*(x1*x))/Bi);
    }
};

class eigenfunction5_1
{
    public:
    eigenfunction5_1(Hypervapotron &hyp);
    double H1;
    double Bi;
    double operator() (const double x)
    {
        return cos(H1*x) - ((sin(H1*x)*(H1*x))/Bi);
    }
};

#endif