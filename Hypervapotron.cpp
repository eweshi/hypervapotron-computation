#include "Hypervapotron.h"
#include "math.h"

using namespace boost::math::tools;
using namespace std;

Hypervapotron::Hypervapotron(int termConvergence, HypParameters hypParameters) : convergeNum(termConvergence), colLoc(0), maxIndex(termConvergence-1)
{
    //ctor

    //Set Arrays to chosen convergence limit
    lambda1_1Vector.resize(convergeNum);
    lambda1_2Vector.resize(convergeNum);
    lambda2_1Vector.resize(convergeNum);
    lambda2_2Vector.resize(convergeNum);
    lambda2_3Vector.resize(convergeNum);
    lambda3_1Vector.resize(convergeNum);
    lambda3_2Vector.resize(convergeNum);
    lambda3_3Vector.resize(convergeNum);
    lambda4_1Vector.resize(convergeNum);
    lambda4_2Vector.resize(convergeNum);
    lambda5_1Vector.resize(convergeNum);

    coefficientMat.resize(convergeNum*8,convergeNum*8); //There are 8 usable series in this computation. and 8 rows.
    coefficientMat.zeros(); //This is a sparse-matrix

    constMat.resize(convergeNum*8,1);
    constMat.zeros();
    
    w = hypParameters.w;
    t = hypParameters.t; 
    w_c = hypParameters.w_c;
    H = hypParameters.H;
    H_c = hypParameters.H_c;
    Bi_1 = hypParameters.Bi_1;
    Bi_2 = hypParameters.Bi_2; 
    Bi_2 = hypParameters.Bi_3; 
    h_eq = hypParameters.h_eq;
    h1 = hypParameters.h1;
    h2 = hypParameters.h2;
    h3 = hypParameters.h3;
    k1 = hypParameters.k1;
    k2 = hypParameters.k2;
    k3 = hypParameters.k3;
    k4 = hypParameters.k4;
    k5 = hypParameters.k5;
    T_ref = hypParameters.T_ref;
    qdoubleprime_infinity = hypParameters.qdoubleprime_infinity; 
    
    

    //Set geometric parameters
    setParameters();
    

}

Hypervapotron::~Hypervapotron()
{
    //dtor
}



void Hypervapotron::generateLambda1_1(eigenfunction1_1 f)
{
    //root finding method
    //TODO: IF I AM NOT MISTAKEN, THIS LOOP HERE IS REDUNDANT. LOOK INTO IT.
    float root = 0;
    int index = 0;
    //root = lambda1_1rootfind(index, f );
    //Note the critical function above has been commented out today on 05:23:14
    //This is because the functions are difficult to find the root. 
    //Instead I am plotting the function and noting where the roots are prior to 
    //program execution. Then I note the different between the roots, as they 
    //are rather regular. So I will generate roots with this detected pattern
    //for the time being.
    
    //For the current profile the starting root is noted below.
    root = 0.52;
    lambda1_1Vector[0] = root;
    for(int i = 1; i < convergeNum; i++)
    {
        root+= 1.04;
        lambda1_1Vector[i] = root;
    }
    
}
void Hypervapotron::generateLambda1_2()
{
    float lambda = 0;

    for(int i = 0; i < convergeNum; i++)
    {
        int n = 2*i + 1; //To give odd numbers
        lambda1_2Vector[i] = (n * M_PI)/(2*w_c);
    }
}
void Hypervapotron::generateLambda2_1()
{
    float lambda = 0;

    for(int i = 0; i < convergeNum; i++)
    {
        int n = 2*i + 1; //To give odd numbers
        lambda2_1Vector[i] = (n * M_PI)/(2*t);
    }
}
void Hypervapotron::generateLambda2_2()
{
    float lambda = 0;

    for(int i = 0; i < convergeNum; i++)
    {
        int n = 2*i + 1; //To give odd numbers
        lambda2_2Vector[i] = (n * M_PI)/(w-(2*w_c));
    }
}
void Hypervapotron::generateLambda2_3()
{
    float lambda = 0;

    for(int i = 0; i < convergeNum; i++)
    {
        int n = 2*i + 1; //To give odd numbers
        lambda2_3Vector[i] = (n * M_PI)/(w-(2*w_c));
    }
}
void Hypervapotron::generateLambda3_1(eigenfunction3_1 f )
{
    //lambda3_1rootfind(1,f);
    //Note the critical function above has been commented out today on 05:23:14
    //This is because the functions are difficult to find the root. 
    //Instead I am plotting the function and noting where the roots are prior to 
    //program execution. Then I note the different between the roots, as they 
    //are rather regular. So I will generate roots with this detected pattern
    //for the time being.
    
    //For the current profile the starting root is noted below.
    
    float root = 0.31;
    lambda3_1Vector[0] = root;
    for(int i = 0; i < convergeNum; i++)
    {
        root += 0.63;
        lambda3_1Vector[i] = root;
    }
    
}
void Hypervapotron::generateLambda3_2(eigenfunction3_1)
{
    //call root finder or copy from lambda 3_1 as it is equal
    lambda3_2Vector = lambda3_1Vector;
}
void Hypervapotron::generateLambda3_3()
{
    //TODO: why is there a lambda 3_3? there is none in the paper.
}
void Hypervapotron::generateLambda4_1()
{
    float lambda = 0;

    for(int i = 0; i < convergeNum; i++)
    {
        int n = 2*i + 1; //To give odd numbers
        lambda4_1Vector[i] = (n * M_PI)/((w/2)-(w_c));
    }
}

void Hypervapotron::generateLambda4_2()
{
    float lambda = 0;

    for(int i = 0; i < convergeNum; i++)
    {
        int n = i + 1; //To give odd numbers
        lambda4_2Vector[i] = (n * M_PI)/(H);
    }
}
void Hypervapotron::generateLambda5_1(eigenfunction5_1 f)
{
   // lambda5_1rootfind(1,f);
   //Under the current profile, this function seems to turn to straight vertical lines.
   //Check the equation given in paper. The current version of this function is simply
   //for testing purposes.
    
    //lambda5_1rootfind(1,f);
    //Note the critical function above has been commented out today on 05:23:14
    //This is because the functions are difficult to find the root. 
    //Instead I am plotting the function and noting where the roots are prior to 
    //program execution. Then I note the different between the roots, as they 
    //are rather regular. So I will generate roots with this detected pattern
    //for the time being.
    
    //For the current profile the starting root is noted below.
    
    float root = 0.02;
    lambda5_1Vector[0] = root;
    for(int i = 0; i < convergeNum; i++)
    {
        root += 0.24;
        lambda5_1Vector[i] = root;
    }
   
    
}

    //TODO: Remove unneeded and redundant function parameters
    //TODO: Workout needed mechanics of placing constants and coefficients in the right matrix
    //Domain 1 Coefficient and Constant functions for matricies.setParameters();

float Hypervapotron::domain1_1coff(int i)
{
    float unknowncoff = 0;
    float constant = 0;
    float lambda_I_1_n = lambda1_1Vector[lamb1_1Count];
    float lambda_II_1_n = lambda2_1Vector[i];
    float x = w_c;
    //Assigning local values from hypervapotron geometry structure and parameters.
   // int i = myHyp.x;


    //SHOWN BELOW IS THE OLD EXPRESSION.
    //I DERIVED A NEW ONE BUT IT MAY BE EQUIVALENT.
    //CHECK PLEASE - 05:25:14
////    constant = (pow(M_E,lambda_II_1_n * x) + pow(M_E,lambda_II_1_n * (w-x))) //M_E == natural e
////                *( ((sin((lambda_II_1_n - lambda_I_1_n)*t)/(2*(lambda_II_1_n - lambda_I_1_n)))
////                  +((sin((lambda_II_1_n + lambda_I_1_n)*t)/(2*(lambda_II_1_n + lambda_I_1_n))))));
////
////    constant /= 2*(cosh(lambda_I_1_n * w_c)) * ((t/2)+ ((sin(2*lambda_I_1_n * t))/(4 * lambda_I_1_n)));
    
    constant = (pow(M_E,lambda_II_1_n * x) + pow(M_E,lambda_II_1_n * (w-x)));
    
    constant *= (lambda_II_1_n*sin(lambda_II_1_n*t)*cos(lambda_I_1_n*t)-lambda_I_1_n*cos(lambda_II_1_n*t)*sin(lambda_I_1_n*t));
    
    constant /= (pow(lambda_II_1_n,2) - pow(lambda_I_1_n,2));
    
    constant /= (((pow(M_E,lambda_I_1_n * x) + pow(M_E,-lambda_II_1_n * x))) 
            * (((2*lambda_I_1_n*t)+ sin(2*lambda_I_1_n*t))/(4*lambda_I_1_n)));
    

    return constant;
}

float Hypervapotron::domain1_2eq(int i)
{


    float constant = 0;
    float lambda1_2 = lambda1_2Vector[i];
    //Assigning local values from hypervapotron geometry structure and parameters.


    constant = (T_ref * (4*t/i*M_PI) * sin(lambda1_2 * w_c)) / (i*M_PI * (1-pow(M_E,2*lambda1_2)*Bi_wc_n)); // check the second lambda term

// This function depends on n so figure that out first.
    return constant;
}

float Hypervapotron::domain2_1coeff(int i)
{

    float constant = 0;
    float unknowncoff = 0;
    float lambda_I_1_n = lambda1_1Vector[i];
    float lambda_II_1_n = lambda2_1Vector[lamb2_1Count];
    

    //Assigning local values from hypervapotron geometry structure and parameters.
   // int i = myHyp.x;


//    constant = (pow(M_E,lambda_I_1_n * w_c) + pow(M_E,-lambda_I_1_n * w_c)) //M_E == natural e
//                *( ((sin((lambda_I_1_n - lambda_II_1_n)*t)/(2*(lambda_I_1_n - lambda_II_1_n)))
//                  +((sin((lambda_I_1_n + lambda_II_1_n)*t)/(2*(lambda_I_1_n + lambda_II_1_n))))));
//
//
//    constant /= (pow(M_E,lambda_II_1_n*w_c) + pow(M_E,lambda_II_1_n*(w-w_c))) * (t/2);
    //New Expression 05:27:14
    constant = (pow(M_E,lambda_I_1_n * w_c) + pow(M_E,-lambda_I_1_n * w_c));  //M_E == natural e
    constant *= (lambda_II_1_n*sin(lambda_II_1_n*t)*cos(lambda_I_1_n*t)-lambda_I_1_n*cos(lambda_II_1_n*t)*sin(lambda_I_1_n*t));
    constant /= (lambda_II_1_n*lambda_II_1_n - lambda_I_1_n*lambda_I_1_n);
    
    constant /= ((2*lambda_II_1_n*t)+ sin(2*lambda_II_1_n*t))/(4*lambda_II_1_n);
    constant /= (pow(M_E,lambda_II_1_n * w_c) + pow(M_E,-lambda_II_1_n * (w - w_c)));
    
    
    return constant;
}

float Hypervapotron::domain2_1const(int i)
{
    //Add the E_I_2_n Constant
    //Keep in mind the lambda is not the same here. It belongs to a different subdomain'

    float constant = 0;
    float unknowncoff = 0;
    float lambda_I_1_n = lambda1_1Vector[i];
    float lambda_II_1_n = lambda2_1Vector[i];
    float lambda_I_2_n = lambda1_2Vector[i];

    //Assigning local values from hypervapotron geometry structure and parameters.
    // int i = myHyp.x;


    constant = cos(lambda_I_1_n*w_c) * Bi_wc_n *
                (((pow(M_E,lambda_I_2_n)*(lambda_I_1_n*cos(lambda_II_1_n*t)+lambda_II_1_n*sin(lambda_II_1_n*t))) + lambda_I_2_n)/(pow(lambda_I_2_n,2)+pow(lambda_II_1_n,2))
                + ((pow(M_E,lambda_II_1_n)*(lambda_II_1_n*sin(lambda_II_1_n*t)-lambda_I_2_n*cos(lambda_II_1_n*t))) + lambda_I_2_n)/(pow(lambda_I_2_n,2)+pow(lambda_II_1_n,2)));

    constant /= (pow(M_E,lambda_II_1_n*w_c) + pow(M_E,lambda_II_1_n*(w-w_c))) * (t/2);

    return constant;
}


float Hypervapotron::domain2_2_const(int i)
{
    float constant = 0;
    float qdoubleprime = qdoubleprime_infinity;
    float lambda2_2 = lambda2_1Vector[i];
    
//    constant = (-qdoubleprime/lambda2_2*k2*(1+pow(M_E,2*t*lambda2_2))) * ((sin(lambda2_2*x1)*(w/2-w_c))/lambda2_2);
//    constant /= (w/2-w_c)/2;
    
    constant = qdoubleprime*sin(lambda2_2*x1)*4*lambda2_2;
    constant /= lambda2_2*lambda2_2*k2*(1+pow(M_E,2*t*lambda2_2))*(2*lambda2_2*x1+sin(2*lambda2_2*x1));
    constant *= (-1);

    return constant;
}

float Hypervapotron::domain2_3_1coeff(int i)
{
    float constant = 0;
    float lambda2_3 = lambda2_3Vector[lamb2_3_1Count];
    float lambda3_1 = lambda3_1Vector[i];
    float y = t;
    
    float a = 0;
    float b = 0;

    //OLD EXPRESSION
//    constant = (1/pow(M_E,lambda2_3*t)+pow(M_E,-lambda2_3*t)) *
//                (pow(M_E,lambda3_1*t)-pow(M_E,-lambda3_1*(2*t-2*H_c-y))) * (1/pow(lambda3_1,2)-pow(lambda2_3,2));
//    constant *= lambda3_1*sin(lambda3_1*x1)*cos(lambda2_3*x1)-lambda2_3*cos(lambda3_1*x1)*sin(lambda2_3*x1);
//
//    constant /= (1/((x1/2)+(sin(2*lambda2_3*x1)/4*lambda2_3)));
    
    //NEW EXPRESSION 05:28:14 //LAMBDA 3_1 == LAMBDA 3_2
    a = ( pow(M_E,lambda3_1*y) - pow(M_E,-lambda3_1*((2*t)+(2*H_c)-y)) );
    a *= (lambda3_1*sin(lambda3_1*x1)*cos(lambda2_3*x1)- lambda2_3*cos(lambda3_1*x1)*sin(lambda2_3*x1));
    a /= (lambda3_1*lambda3_1) - (lambda2_3*lambda2_3);
    

    
    //ERROR STOP PLEASE 
    //THERE SEEEMS TO HAVE BEEN A MISTAKE HERE PLEASE LOOK AGAIN AT THE STRUCTURE 
    
    constant = a;   
    constant *= 1/(pow(M_E,lambda2_3*t)+pow(M_E,-lambda2_3*t));
    constant /= (2*lambda2_3*x1 + sin(2*lambda2_3*x1))/(4*lambda2_3);
    
    
    return constant;
}

float Hypervapotron::domain2_3_2coeff(int i)
{
    float lambda3_1 = lambda3_1Vector[i];
    float lambda3_2 = lambda3_2Vector[i];
    float lambda2_3 = lambda2_3Vector[lamb2_3_1Count]; //IS IT LAMB2_3_1 OR LAMB2_3_2 PLEASE CHECK
    //TODO: CHECK IF LAMB2_3_1 OR LAMB2_3_2
    
    float b = 0;
    float constant = 0;
    float y = t;
    
//    constant = pow(M_E,lambda3_1*t)-pow(M_E,lambda3_1*(2*t-t)) * (1/pow(lambda3_2,2)-pow(lambda2_3,2));
//    constant *= lambda3_2*sin(lambda3_2*x1)*cos(lambda2_3*x1) - lambda2_3*cos(lambda3_2*x1)*sin(lambda2_3*x1);
//    constant /= (1/((x1/2)+(sin(2*lambda2_3*x1)/4*lambda2_3)));
    
    b = ( pow(M_E,lambda3_1*y) - pow(M_E,-lambda3_1*((2*t)-y)) );
    b *= (lambda3_1*sin(lambda3_1*x1)*cos(lambda2_3*x1)- lambda2_3*cos(lambda3_1*x1)*sin(lambda2_3*x1));
    b /= (lambda3_1*lambda3_1) - (lambda2_3*lambda2_3);
    
    constant = b;
    constant *= 1/(pow(M_E,lambda2_3*t)+pow(M_E,-lambda2_3*t));
    constant /= (2*lambda2_3*x1 + sin(2*lambda2_3*x1))/(4*lambda2_3);
    
    return constant;
}
//TODO: Add Domain 2_3 which contains two unknown coff series
//TODO: Verify that the two different lambda 3's in domain2_3_2coeff function are right


float Hypervapotron::domain3_1_1coeff(int i)
{
    float constant = 0;

    float lambda2_1 = lambda2_1Vector[i];
    float lambda2_2 = lambda2_2Vector[i];
    float lambda3_1 = lambda3_1Vector[lamb3_1Count];
    float x = x1;
    float y = t;
    //TODO: Going to assume that this x is equal to x1

    //OLD EXPRESSION
//    constant = (pow(M_E,lambda2_1*x)+pow(M_E,lambda2_1*(w-x)));
//    constant *= (sin(lambda3_1*x1)*cos(lambda2_2*y))/lambda3_1;
//    constant *= lambda3_1/(sin(lambda3_1*x1)*(pow(M_E,lambda3_1*t)-pow(M_E,lambda3_1*(2*H_c+t))));
    
    //Domain 3_1 NEW EXPRESSION 05:23:2014
    constant = pow(M_E,lambda2_1*(w-(w/2)));
    constant *= pow(M_E,-lambda2_1*x1) * (((lambda3_1*sin(lambda3_1*x1)-lambda2_1*cos(lambda2_1*x1)+lambda2_1*pow(M_E,lambda2_1*x1))))/(((lambda3_1*lambda3_1)+(lambda2_1*lambda2_1)));
    constant *= cos(lambda2_1*y);
    constant *= 2;
    constant /= ((2*lambda3_1*x)+sin((2*lambda3_1*x)))/(4*lambda3_1);
    
    constant *= (1 / (pow(M_E,lambda3_1*t)-pow(M_E,lambda3_1*(2*H_c+t))));
    
    return constant;
}

float Hypervapotron::domain3_1_2const(int i)
{
    float constant = 0;
    float lambda2_2 = lambda2_2Vector[i];
    float lambda2_3 = lambda2_3Vector[i];
    float lambda3_1 = lambda3_1Vector[lamb3_1Count];
    float y = t ;
    constant = (pow(M_E,lambda2_3)+pow(M_E,-lambda2_3*y));
    constant *= (lambda3_1*sin(lambda3_1*x1)*cos(lambda2_2*x1)-lambda2_2*cos(lambda3_1*x1)*sin(lambda2_2*x1))
                    /(pow(lambda3_1,2)-pow(lambda2_2,2));
    constant *= lambda3_1/(sin(lambda3_1*x1)*(pow(M_E,lambda3_1*t)-pow(M_E,lambda3_1*(2*H_c+t))));
    return constant;
}

float Hypervapotron::domain3_1_3coeff(int i)
{
    //TODO: Add E_II_2_n to constant
    //Add mechanism to fetch E_II_2_n from a list or something
    float constant = 0;
    float x = x1;
    float y = t;

    float lambda2_3 = lambda2_3Vector[i];
    float lambda3_1 = lambda3_1Vector[lamb3_1Count];
    
    constant = (pow(M_E,lambda2_3*y) + pow(M_E,-lambda2_3*y));
    constant *= (lambda3_1*sin(lambda3_1*x1) * cos(lambda2_3*x1)-lambda2_3*cos(lambda3_1*x1)*sin(lambda2_3*x1))
            /((lambda3_1*lambda3_1)-(lambda2_3*lambda2_3));
    
    constant /= ((2*lambda3_1*x)+sin((2*lambda3_1*x)))/(4*lambda3_1);
    
    constant *= (1 / (pow(M_E,lambda3_1*t)-pow(M_E,lambda3_1*(2*H_c+t))));
    
    return constant;
}

float Hypervapotron::domain3_2_1coeff(int i)
{
    float constant = 0;

    float lambda3_1 = lambda3_1Vector[i];
    float lambda4_1 = lambda4_1Vector[i];

    constant = (4*lambda3_1)/((2*lambda3_1*x1+sin(2*lambda3_1*x1))*(pow(M_E,lambda3_1*(t+2*H_c))-pow(M_E,lambda3_1*(t-2*H_c))));

    constant *= cosh(lambda4_1*y1) * (lambda4_1*sin(lambda4_1*x1)*cos(lambda3_1*x1)-lambda3_1*cos(lambda4_1*x1)*sin(lambda3_1*x1))
                    /(pow(lambda4_1,2)-pow(lambda3_1,2));

    return constant;
}

float Hypervapotron::domain3_2_2coeff(int i)
{
    float constant = 0;

    float lambda3_1 = lambda3_1Vector[i];
    float lambda4_2 = lambda4_2Vector[i];

    constant = (4*lambda3_1)/((2*lambda3_1*x1+sin(2*lambda3_1*x1))*(pow(M_E,lambda3_1*(t+2*H_c))-pow(M_E,lambda3_1*(t-2*H_c))));

    constant *= cosh(lambda4_2*y1) * (lambda4_2*sinh(lambda4_2*x1)*cos(lambda3_1*x1)+lambda3_1*cosh(lambda4_2*x1)*sin(lambda3_1*x1))
                    /(pow(lambda4_2,2)-pow(lambda3_1,2));

    return constant;
}


float Hypervapotron::domain4_1_1coeff(int i)
{
    float constant = 0;

    float lambda3_1 = lambda3_1Vector[i];
    float lambda4_1 = lambda4_1Vector[i];
    float y = t;

    constant = -(4*lambda4_1)/((cosh(lambda4_1*H_1))*(2*lambda4_1*x1+sin(2*lambda4_1*x1)));
    constant *= (lambda3_1 * pow(M_E,lambda3_1*y)+pow(M_E,lambda3_1*(2*t+2*H_c-y)));

    constant *= (lambda4_1*sin(lambda4_1*x1)*cos(lambda3_1*x1)-lambda3_1*cos(lambda4_1*x1)*sin(lambda3_1*x1))
                    /(pow(lambda4_1,2)-pow(lambda3_1,2));
    return constant;
}

float Hypervapotron::domain4_1_2coeff(int i)
{
    float constant = 0;
    float lambda3_2 = lambda3_2Vector[i];
    float lambda4_1 = lambda4_1Vector[i];
    float y =t ;

    constant = -(4*lambda4_1)/((cosh(lambda4_1*H_1))*(2*lambda4_1*x1+sin(2*lambda4_1*x1)));
    constant *= (lambda3_2 * pow(M_E,lambda3_2*y)+pow(M_E,lambda3_2*(2*t-y)));

    constant *= (lambda4_1*sin(lambda4_1*x1)*cos(lambda3_2*x1)-lambda3_2*cos(lambda4_1*x1)*sin(lambda3_2*x1))
                    /(pow(lambda4_1,2)-pow(lambda3_2,2));
    return constant;
}

float Hypervapotron::domain4_2_1coeff(int i)
{
    float constant = 0;
    float lambda4_2=lambda4_2Vector[i];
    float lambda5=lambda5_1Vector[i];


    constant = (4*lambda4_2)/((cosh(lambda4_2*(w/2-w_c)))*(2*lambda4_2*y1+sin(2*lambda4_2*y1)));

    constant *= cosh(lambda5*x1)*(lambda5*sin(lambda5*y1)*cos(lambda4_2*y1)-lambda4_2*cos(lambda5*y1)*sin(lambda4_2*y1))
                    /(pow(lambda5,2)-pow(lambda4_2,2));

    return constant;
}


//TODO Check thse two coeff functions they seem incorrect.
float Hypervapotron::domain5_1_1coeff(int i)
{
    float constant = 0;
    float lambda4_1=lambda4_1Vector[i];
    float lambda5=lambda5_1Vector[i];

    constant = (4*k4)/((k5*sinh(lambda5*w_c))*(2*lambda5*y1+sin(2*lambda5*y1)));

    constant *= lambda4_1*sin(lambda4_1*x1);
    constant *= (lambda5*sin(lambda5*y1)*cosh(lambda4_1*y1)+lambda4_1*cos(lambda5*y1)*sinh(lambda4_1*y1))
                    /(pow(lambda5,2)+pow(lambda4_1,2));
    return constant;
}

float Hypervapotron::domain5_1_2coeff(int i)
{
    float constant = 0;

    float lambda4_2 = lambda4_2Vector[i];
    float lambda5 = lambda5_1Vector[i];

    constant = -(4*k4)/((k5*sinh(lambda5*w_c))*(2*lambda5*y1+sin(2*lambda5*y1)));

    constant *= lambda4_2*sin(lambda4_2*x1);
    constant *= (lambda5*sin(lambda5*y1)*cos(lambda4_2*y1)-lambda4_2*cos(lambda5*y1)*sin(lambda4_2*y1))
                    /(pow(lambda5,2)-pow(lambda4_2,2));

    return constant;
}

float Hypervapotron::lambda1_1rootfind(int index, eigenfunction1_1 f)
{
    //the object for this test is to test
    //the root finding with boost.

    typedef std::pair<double, double> Result;
    boost::uintmax_t max_iter=1000;
    boost::math::tools::eps_tolerance<double> tol(300);
    double lowBound = 0.05;
    double hiBound = 2;


    Result result1;


    lowBound = 0;
    hiBound = 0.7;
            
    result1 = toms748_solve(f,lowBound,hiBound,tol,max_iter);
    //I have tested this code with this function and it gives accurate results to atleast 1000 terms.
    //Further testing has not been done.
    lambda1_1Vector[0] = result1.second; // Get proper value from result
    for(int i = 1; i < convergeNum; i++)
    {
        lowBound = result1.second + 0.5;
        hiBound = lowBound + 0.9;
        result1 = toms748_solve(f,lowBound,hiBound,tol,max_iter);

        //cout << "First value is " << result1.first << endl;
        lambda1_1Vector[i] = result1.second;
        cout << endl;
    }
    
}
float Hypervapotron::lambda3_1rootfind(int index,eigenfunction3_1 f)
{
    //the object for this test is to test
    //the root finding with boost.

    typedef std::pair<double, double> Result;
    boost::uintmax_t max_iter=1000;
    boost::math::tools::eps_tolerance<double> tol(300);
    double lowBound = 0.05;
    double hiBound = 2;


    Result result1;


    lowBound = 0;
    hiBound = 0.6;

    result1 = toms748_solve(f,lowBound,hiBound,tol,max_iter);
    lambda3_1Vector[0] = result1.second;
    //I have tested this code with this function and it gives accurate results to atleast 1000 terms.
    //Further testing has not been done.

    for(int i = 1; i < convergeNum; i++)
    {
        lowBound = result1.second + 0.2;
        hiBound = lowBound + 0.8;
        result1 = toms748_solve(f,lowBound,hiBound,tol,max_iter);

        cout << "First value is " << result1.first << endl;
        lambda3_1Vector[i] = result1.second;
        cout << endl;
    }
}
float Hypervapotron::lambda5_1rootfind(int index, eigenfunction5_1 f )
{
    //the object for this test is to test
    //the root finding with boost.

    typedef std::pair<double, double> Result;
    boost::uintmax_t max_iter=1000;
    boost::math::tools::eps_tolerance<double> tol(300);
    double lowBound = 0.05;
    double hiBound = 2;


    Result result1;


    lowBound = 0;
    hiBound = 2;

    result1 = toms748_solve(f,lowBound,hiBound,tol,max_iter);
    lambda5_1Vector[0] = result1.second;
    //I have tested this code with this function and it gives accurate results to atleast 1000 terms.
    //Further testing has not been done.

    for(int i = 1; i < convergeNum; i++)
    {
        lowBound = result1.second + 1.2;
        hiBound = result1.second + 3.2;
        result1 = toms748_solve(f,lowBound,hiBound,tol,max_iter);

        cout << "First value is " << result1.first << endl;
        lambda5_1Vector[i] = result1.second;
        cout << endl;
    }
}

void Hypervapotron::beginComputation(eigenfunction1_1 efunc1, eigenfunction3_1 efunc3, eigenfunction5_1 efunc5)
{
     //Generate Lambda values.
     setParameters();
     generateLambda1_1(efunc1);
     generateLambda1_2();
     generateLambda2_1();
     generateLambda2_2();
     generateLambda2_3();
     generateLambda3_1(efunc3);
     generateLambda3_2(efunc3);
     generateLambda3_3();
     generateLambda4_1();
     generateLambda4_2();
     generateLambda5_1(efunc5);



    //Construct matrix row by row.
     for(int i = 0; i < convergeNum; i++)
     {
         makeMatrix(i);
         
         B = arma::solve(coefficientMat,constMat); // Obtain Vector solutions
         cout << coefficientMat << endl << constMat << endl;
         
         
         //***Add loop here from 0 to term num 
         //****For this is only for the case n = 1;
         d1_1Vec.push_back(B[0]);
         d1_2Vec.push_back(domain1_2eq(i));
         d2_1Vec.push_back(B[1]);
         d2_2Vec.push_back(domain2_2_const(i));
         d2_3Vec.push_back(B[2]);
         d3_1Vec.push_back(B[3]);
         d3_2Vec.push_back(B[4]);
         d4_1Vec.push_back(B[5]);
         d4_2Vec.push_back(B[6]);
         d5_1Vec.push_back(B[7]);
         
         cout << endl;
     }

}

void Hypervapotron::makeRow1(int index)
{
    //Row 1 does not have a constant on the right hand side. No Contribution to const column vector.
    coefficientMat(0,0) = 1;
    coefficientMat(0,1) = -(domain1_1coff(index)); //That is mathematically, (1,1)  
}

void Hypervapotron::makeRow2(int index)
{
    //Domain 2_2 is a constant providing series in this row
    //The other two domains contribute to the coefficient matrix
    
    coefficientMat(1,0) = -(domain2_1coeff(index));
    coefficientMat(1,1) = 1;
    
    constMat(1) = domain2_1const(index); 
}

void Hypervapotron::makeRow3(int index)
{
    coefficientMat(2,3) = 1;
    coefficientMat(2,4) = -(domain2_3_1coeff(index));
    coefficientMat(2,5) = -(domain2_3_2coeff(index));
}

void Hypervapotron::makeRow4(int index)
{
    coefficientMat(3,1) = -(domain3_1_1coeff(index));
    coefficientMat(3,2) = -(domain3_1_3coeff(index));
    coefficientMat(3,3) = 1;
    
    constMat(3) = domain3_1_2const(index);
}

void Hypervapotron::makeRow5(int index)
{
    coefficientMat(4,4) = 1;
    coefficientMat(4,5) = -(domain3_2_1coeff(index));
    coefficientMat(4,6) = -(domain3_2_2coeff(index));
}

void Hypervapotron::makeRow6(int index) 
{
    //TODO We need to make sure and see if these functions need a negative
    //in the front. Check integral derivations.
    
    coefficientMat(5,3) = domain4_1_1coeff(index);
    coefficientMat(5,4) = domain4_1_2coeff(index);
    coefficientMat(5,5) = 1;
}

void Hypervapotron::makeRow7(int index) 
{
    coefficientMat(6,6) = 1;
    coefficientMat(6,7) = -(domain4_2_1coeff(index));
}

void Hypervapotron::makeRow8(int index) 
{
    coefficientMat(7,5) = -(domain5_1_1coeff(index));
    coefficientMat(7,6) = -(domain5_1_2coeff(index));
    coefficientMat(7,7) = 1; //Done. 
}

void Hypervapotron::makeMatrix(int index) 
{
    makeRow1(index);
    makeRow2(index);
    makeRow3(index);
    makeRow4(index);
    makeRow5(index);
    makeRow6(index);
    makeRow7(index);
    makeRow8(index);
}


void Hypervapotron::setParameters()
{
    x1 = (w/2) - w_c;
    H_1 = H - (2 * H_c) - t;
    y1 = H_1;
}

void Hypervapotron::solveCoefficients(eigenfunction1_1 efunc1,eigenfunction3_1 efunc3 ,eigenfunction5_1 efunc5) 
{
    generateLambdaVectors(efunc1,efunc3,efunc5);
    fillMatrixSystem();
    solveSystem();
    
    cout << coefficientMat << endl;
}

void Hypervapotron::fillMatrixSystem() 
{
    //Fill in known Coefficients First
    fillDomain1_2();
    fillDomain2_2();
    
    //Fill Coefficient Matrix
    fillSection1();
    fillSection2();
    fillSection3();
    fillSection4();
    fillSection5();
    fillSection6();
    fillSection7();
    fillSection8();
    
    //Fill Constant Matrix
    
}

void Hypervapotron::fillSection1() 
{
    //Begin to fill Section 1
    col = 0; 
    //Term changing here is the lambda1_1
    lamb1_1Count = 0;
    for(row = 0; row < convergeNum; row++)
    {
        //Set 1 Term
        coefficientMat(row,col) = 1;
        
        //Set Constant Expressions
        
        //Coefficient Matrix
        //The limits on this loops were determined by 
        //inspection of the 
        int counter = 0;
        for(int k = (convergeNum); k < (2*convergeNum);k++)
        {
            coefficientMat(row,k) = domain1_1coff(counter);
            counter++;
        }
        
       
        
        col++;
        lamb1_1Count++;
    }
}

void Hypervapotron::fillSection2() 
{
    //Begin to fill Section 2
    lamb2_1Count = 0;
    
    for ( ; row < convergeNum*2; row++)
    {
        //Set 1 Term
        coefficientMat(row,col) = 1;
        
        //Set Constant Expressions
        //The limits on this loops were determined by 
        //inspection of the 
        int counter = 0;
        for(int k = 0; k < convergeNum ;k++)
        {
            coefficientMat(row,k) = domain2_1coeff(counter);
            counter++;
        }
        
        //Constant Vector
        //is a counter reset needed here?
        counter = 0;
        for(; counter < convergeNum; counter++)
        {
             constMat(row) += domain2_1const(counter);
        }
        
        
        col++;
        lamb2_1Count++;
    }
}

void Hypervapotron::fillSection3() 
{
    lamb2_3_1Count = 0;
    lamb2_3_2Count = 0;
    for ( ; row < convergeNum*3; row++)
    {
        //Set 1 Term
        coefficientMat(row,col) = 1;
        
        //Set Constant Expressions
        //The limits on this loops were determined by 
        //inspection of the 
        int counter = 0;
        for(int k = (convergeNum*3) ; k < (convergeNum*4) ;k++)
        {
            coefficientMat(row,k) = domain2_3_1coeff(counter);
            counter++;
        }
        
        counter = 0;
        for(int k = (convergeNum*4) ; k < (convergeNum*5) ;k++)
        {
            coefficientMat(row,k) = domain2_3_2coeff(counter);
            counter++;
        }
        
        lamb2_3_1Count++;
        col++;
    }
}

void Hypervapotron::fillSection4() 
{
    lamb3_1Count = 0;
    
    for ( ; row < convergeNum*4; row++)
    {
        //Set 1 Term
        coefficientMat(row,col) = 1;
        
        //Set Constant Expressions
        //The limits on this loops were determined by 
        //inspection of the 
        int counter = 0;
        for(int k = (convergeNum) ; k < (convergeNum*2) ;k++)
        {
            coefficientMat(row,k) = domain3_1_1coeff(counter);
            counter++;
        }
        
        counter = 0;
        for(int k = (convergeNum*2) ; k < (convergeNum*3) ;k++)
        {
            coefficientMat(row,k) = domain3_1_3coeff(counter);
            counter++;
        }
        
        //Constant Vector
        counter = 0;
        for(; counter < convergeNum; counter++)
        {
             constMat(row) += domain3_1_2const(counter);
        }

        lamb3_1Count++;
        col++;
    }
}

void Hypervapotron::fillSection5() 
{
    for ( ; row < convergeNum*5; row++)
    {
        //Set 1 Term
        coefficientMat(row,col) = 1;
        
        //Set Constant Expressions
        //The limits on this loops were determined by 
        //inspection of the 
        int counter = 0;
        for(int k = (convergeNum*5) ; k < (convergeNum*6) ;k++)
        {
            coefficientMat(row,k) = domain3_2_1coeff(counter);
            counter++;
        }
        
        counter = 0;
        for(int k = (convergeNum*6) ; k < (convergeNum*7) ;k++)
        {
            coefficientMat(row,k) = domain3_2_2coeff(counter);
            counter++;
        }
        
        col++;
    }
}

void Hypervapotron::fillSection6() 
{
    for( ; row < convergeNum*6; row++)
    {
        //Set 1 Term
        coefficientMat(row,col) = 1;
        
        //Set Constant Expressions
        //The limits on this loops were determined by 
        //inspection of the 
        int counter = 0;
        for(int k = (convergeNum*3) ; k < (convergeNum*4) ;k++)
        {
            coefficientMat(row,k) = domain4_1_1coeff(counter);
            counter++;
        }
        
        counter = 0;
        for(int k = (convergeNum*4) ; k < (convergeNum*5) ;k++)
        {
            coefficientMat(row,k) = domain4_1_1coeff(counter);
            counter++;
        }
        
        col++;
    }
}

void Hypervapotron::fillSection7() 
{
     for( ; row < convergeNum*7; row++)
    {
        //Set 1 Term
        coefficientMat(row,col) = 1;
        
        //Set Constant Expressions
        //The limits on this loops were determined by 
        //inspection of the 
        int counter = 0;
        for(int k = (convergeNum*7) ; k < (convergeNum*8) ;k++)
        {
            coefficientMat(row,k) = domain4_2_1coeff(counter);
            counter++;
        }
        
        
        col++;
    }
}

void Hypervapotron::fillSection8() 
{
    for( ; row < convergeNum*8; row++)
    {
        //Set 1 Term
        coefficientMat(row,col) = 1;
        
        //Set Constant Expressions
        //The limits on this loops were determined by 
        //inspection of the 
        int counter = 0;
        
        for(int k = (convergeNum*5) ; k < (convergeNum*6) ;k++)
        {
            coefficientMat(row,k) = domain5_1_1coeff(counter);
            counter++;
        }
        
        counter = 0;
        
        for(int k = (convergeNum*6) ; k < (convergeNum*7) ;k++)
        {
            coefficientMat(row,k) = domain5_1_2coeff(counter);
            counter++;
        }
        
        col++;
    }
}

void Hypervapotron::solveSystem() 
{
    cout << coefficientMat << endl << constMat << endl;
     B = arma::solve(coefficientMat,constMat); // Obtain Vector solutions
 
         
         //PLEASE EXAMINE THE INDEXES HERE
         //***Add loop here from 0 to term num 
         //****For this is only for the case n = 1;
         d1_1Vec.push_back(B[0]);
     //    d1_2Vec.push_back(domain1_2eq(i));
         d2_1Vec.push_back(B[1]);
      //   d2_2Vec.push_back(domain2_2_const(i));
         d2_3Vec.push_back(B[2]);
         d3_1Vec.push_back(B[3]);
         d3_2Vec.push_back(B[4]);
         d4_1Vec.push_back(B[5]);
         d4_2Vec.push_back(B[6]);
         d5_1Vec.push_back(B[7]);
         cout << endl <<"Printing B " << endl;
         cout << B << endl;
         cout << endl;
        
}

void Hypervapotron::generateLambdaVectors(eigenfunction1_1 efunc1 ,eigenfunction3_1 efunc3 ,eigenfunction5_1 efunc5)
{
     generateLambda1_1(efunc1);
     generateLambda1_2();
     generateLambda2_1();
     generateLambda2_2();
     generateLambda2_3();
     generateLambda3_1(efunc3);
     generateLambda3_2(efunc3);
     generateLambda3_3();
     generateLambda4_1();
     generateLambda4_2();
     generateLambda5_1(efunc5);
}

void Hypervapotron::fillDomain1_2() 
{
    for(int i = 0; i < convergeNum; i++)
    {
        d1_2Vec.push_back(domain1_2eq(i));
    }

}

void Hypervapotron::fillDomain2_2() 
{
    for(int i = 0; i < convergeNum; i++)
    {
        d2_2Vec.push_back(domain2_2_const(i));
    }

}








