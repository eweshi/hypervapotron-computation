#include <iostream>
#include "Hypervapotron.h"
#include "eigenfunction.h"
#include "HypParameters.h"
#define  ARMA_DONT_USE_WRAPPER
#include <armadillo>
#include <boost/math/tools/roots.hpp>

using namespace std;
using namespace arma;


int main()
{

    //PLEASE CONSIDER THIS CAUTION
    //THE EIGENVALUE FUNCTIONS MUST BE CHARACTERIZED
    //BEFOREHAND. I.E. PLOT AND LOOK AT THE WAY THE
    //FUNCTION BEHAVES SO YOU CAN DETERMINE ROOT LOCATIONS.
  int termNumber = 0;
  HypParameters hypParameters;
  
  cout << "Please input the number of terms you wish to evaluate the series to:" << endl;
  cin >> termNumber;
  cout << "Input the width of the geometry." << endl; 
  
  cout << "Input the thickness of the geometry" << endl;

  //TODO: Handle the event where the root finder returns an error. if there is an error, then increase boundary values.
  //or simply look at graph before hand and see the boundaries for yourself.
  //TODO: Establish how many rows the matrix will have. I will initially assume 5 rows.
  //TODO: Step through the computation carefully for all x and y values to see how the computation will procede.

  //TODO: Establish x and y coeff function pattern
  //TODO: Remember now you must include where the domains start in respect to x and y
  //TODO: CHECK why some of the coeff equations do not have an x or y
  //TODO: Set bi and other parameters in setparam function, etc
  //TODO: Check if domain sub subs are correct.
  //TODO: Correct number of colums related to max convergence
  
  //TODO:Implement extraction of hyp parameters from file.
  //TODO:Look at structure of coefficient vector

    //Hypervapotron myhyp(termNumber);
    
    //For now, the parameters will be inputted here.
    //***TODO: GO SET BI_WC_N IN ALL FUNCTIONS THAT IT IS USED. 
    //*****TODO: GO SET THE PROPER INTERVALS FOR ROOT FINDING FUNCTIONS
    //LOOK UP WHAT THE FUNCTIONS LOOK LIKE AND DETERMINE INTERVALS 
    
    
  Hypervapotron myhyp(termNumber,hypParameters);
  eigenfunction1_1 efunc1(myhyp);
  eigenfunction3_1 efunc3(myhyp);
  eigenfunction5_1 efunc5(myhyp);

  myhyp.solveCoefficients(efunc1,efunc3,efunc5);


  //TODO: Correct Scope errors
  //TODO: Implement root finding with library
  //Call construct coeff matrix

  //Call construct constant matrix

  //Solve

  //Store results in new matrix

  //Plot results


  return 0;
}