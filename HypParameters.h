/* 
 * File:   HypParameters.h
 * Author: eweshi
 *
 * Created on May 21, 2014, 1:29 PM
 */

#ifndef HYPPARAMETERS_H
#define	HYPPARAMETERS_H

struct HypParameters 
{
        
    float w = 50;
    float t = 3;
    float w_c = 20;
    float H = 24.5;
    float H_c = 3.5;
    float Bi_1 = 400;
    float Bi_2 = 400;
    float Bi_3 = 400;
    float h_eq = 30000;
 
    float h1 = 30000;
    float h2 = 30000;
    float h3 = 30000;
    float k1=  320;
    float k2 = 320;
    float k3 = 320;
    float k4 = 320;
    float k5 = 320;


    float T_ref = 1;
    float x1;
    float y1;
    float qdoubleprime_infinity = 15000000;

};

#endif	/* HYPPARAMETERS_H */

